from django.shortcuts import render, redirect, get_object_or_404
from tasks.models import Task
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm


@login_required
def project_details(request, id):
    project = get_object_or_404(Project, id=id)
    task = Task.objects.filter(project=id)
    context = {"project_key": project, "details_key": task}
    return render(request, "tasks/details.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm
        context = {"create_task_key": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"my_tasks_key": tasks}
    return render(request, "tasks/my_tasks.html", context)
